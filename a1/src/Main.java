import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //get firstName:
        System.out.println("First Name:");
        String firstName = scanner.nextLine();

        //get lastName
        System.out.println("Last Name:");
        String lastName = scanner.nextLine();

        //get 1st grade
        System.out.println("First Subject Grade:");
        double firstGrade = scanner.nextDouble();

        //get 2nd grade
        System.out.println("Second Subject Grade:");
        double secondGrade = scanner.nextDouble();

        //get 3rd grade
        System.out.println("Third Subject Grade:");
        double thirdGrade = scanner.nextDouble();

        //get average
        double average = (firstGrade + secondGrade + thirdGrade) / 3;
        System.out.println(average);

        //convert average to integer
        /*
        * Explicit Casting/Narrowing is done when converting a larger type to smaller type
        * double -> float -> long -> int -> char -> short -> byte
        * dataType name = (dataType) variable;
        * */
        int intAve = (int) average;
        System.out.println(intAve);
        /*
        * Implicit Casting/Widening is done automatically when converting from smaller to a larger type
        * */

        double dblAve = intAve;

        //display result
        System.out.println("Good day, " +firstName +" " +lastName +".");
        System.out.println("Your grade average is: " +dblAve);
    }
}