import java.util.Scanner;

public class Main {
    /*
    * Main Class
    * The main class is the entry point for our java program.
    * It is responsible for executing our code.
    * The main class is usually has 1 method inside, the main() method. The main method is the method to run our code.*/
    public static void main(String[] args){
        /*
        * Main method is where most executable code is applied to.
        * "public" is an access modifier which simply tells the application which classes have access to our method/attributes
        * "static" means that the method/property belongs to the class. This means that it is accessible without having to create an instance of a class.
        * "void" means that this method will not return data. In java, we have to declare datatype of the methods returned. And since, main does not return data they add "void".*/

        System.out.println("Chaz Ocana");
        System.out.println("I am learning Java");

        //In java, to be able to declare a variable, we have to identify or declare its data type. Which means, that the variable will expect and only access data with the type declared;

        int myNum;
        myNum = 3;
        System.out.println(myNum);

        myNum = 20000;
        System.out.println(myNum);

        //"L" is added at the end of long to be recognized as long
        long nationalPopulation = 3998432423L;
        System.out.println(nationalPopulation);

        //"f" is added at the end to be recognized as float.
        //constant in java is declared with the keyword "final"
        final float pi = 3.14566784f;
        System.out.println(pi);

        Scanner scannerName = new Scanner(System.in);
/*
        System.out.println("Enter your name:");
        String myName = scannerName.nextLine();
        System.out.println("Your name is " +myName);

        System.out.println("What is your favorite number?");
        int myFaveNum = scannerName.nextInt();
        System.out.println(myFaveNum);

        System.out.println("What was your average grade in HS?");
        double averageGrade = scannerName.nextDouble();
        System.out.println(averageGrade);*/

        System.out.println("Give me a number:");
        int num1 = scannerName.nextInt();
        System.out.println("Give me another number:");
        int num2 = scannerName.nextInt();
        int difference = num1 - num2;
        System.out.println("The difference of " +num1 +" and " +num2 +" is " +difference);
    }
}